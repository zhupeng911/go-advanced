package main

// Gin参考文档：https://www.liwenzhou.com/posts/Go/Gin_framework

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

func main() {
	//TestGin001()
	//TestGin002()
	//TestGin003()
	//TestGin004()
	//TestGin005()
	//TestGin006()
	//TestGin007()
	//TestGin008()
	//TestGin009()
	TestGin010()
	//TestGin011()
}
func TestGin011() {
	//engine := gin.Default()
	data := map[string]interface{}{
		"msg": "name",
	}

	println(data)
	//engine.Run()
}

// TestGin010 中间件-类似于AOP，路由过滤等
// 1.Gin框架允许开发者在处理请求的过程中，加入用户自己的钩子（Hook）函数，这个钩子函数就叫中间件。
// 2.中间件适合处理一些公共的业务逻辑，比如登录认证、权限校验、数据分页、记录日志、耗时统计等。
// 3.Gin中的中间件必须是一个gin.HandlerFunc类型

//gin.Default()默认使用了Logger和Recovery中间件，其中：
//Logger中间件将日志写入gin.DefaultWriter，即使配置了GIN_MODE=release。
//Recovery中间件会recover任何panic。如果有panic的话，会写入500响应码。
//如果不想使用上面两个默认的中间件，可以使用gin.New()新建一个没有任何默认中间件的路由。

// ProgramTimeCost a、定义中间件:统计接口耗时的中间件
func ProgramTimeCost() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Printf("中间件---ProgramTimeCost in... \n")
		start := time.Now()
		c.Next()             // 调用该请求的剩余处理程序
		c.Set("name", "小王子") // 可以通过c.Set在请求上下文中设置值，后续的处理函数能够取到该值
		// c.Abort() // 不调用该请求的剩余处理程序
		cost := time.Since(start)
		fmt.Printf("该接口耗时: %v \n", cost)
		fmt.Printf("中间件---ProgramTimeCost out... \n")
	}
}

// TestGin010 b、注册中间件
func TestGin010() {
	r := gin.Default()
	// 注册一个全局中间件，可以一次性注册多个中间件
	r.Use(ProgramTimeCost())
	r.GET("/index", func(c *gin.Context) { c.JSON(http.StatusOK, gin.H{"msg": "index"}) })
	r.GET("/login", func(c *gin.Context) { c.JSON(http.StatusOK, gin.H{"msg": "login"}) })

	// 给路由单独注册中间件（一个接口可注册多个中间件）
	r.GET("/logout", ProgramTimeCost(), func(c *gin.Context) {
		name := c.MustGet("name").(string) // 从中间件上下文取值
		c.JSON(http.StatusOK, gin.H{
			"msg":  "logout",
			"name": name,
		})
	})

	r.Run()
}

// TestGin009 路由组
func TestGin009() {
	r := gin.Default()
	// 1.普通路由
	r.GET("/index", func(c *gin.Context) {})
	r.GET("/login", func(c *gin.Context) {})
	r.POST("/login", func(c *gin.Context) {})
	// 等同于下面Any
	r.Any("/test", func(c *gin.Context) {
		switch c.Request.Method {
		case "GET":
			c.JSON(http.StatusOK, gin.H{"method": "GET"})
		case "POST":
			c.JSON(http.StatusOK, gin.H{"method": "POST"})

		}
	})

	// 2.没有匹配到路由的请求返回统一404页面
	//r.NoRoute(func(c *gin.Context) {
	//	c.JSON(http.StatusNotFound, gin.H{"msg": "404"})
	//})
	r.LoadHTMLFiles("./static/404.html")
	r.NoRoute(func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "404.html", nil)
	})

	//3.路由组 将拥有共同URL前缀的路由划分为一个路由组
	//为路由组注册中间件方法一
	userGroup := r.Group("/user", ProgramTimeCost())
	{
		userGroup.GET("/queryName", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"path": "/user/queryName",
			})
		})
		userGroup.GET("/queryAge", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"path": "/user/queryAge",
			})
		})

		// 路由组也是支持嵌套的
		//为路由组注册中间件方法二
		shopGroup := userGroup.Group("/shop")
		shopGroup.Use(ProgramTimeCost())
		{
			shopGroup.GET("/queryShopName", func(c *gin.Context) {
				c.JSON(http.StatusOK, gin.H{
					"path": "/user/shop/queryShopName",
				})
			})
			shopGroup.GET("/queryShopAddr", func(c *gin.Context) {
				c.JSON(http.StatusOK, gin.H{
					"path": "/user/shop/queryShopAddr",
				})
			})
		}
	}
	r.Run()
}

// TestGin008 重定向
func TestGin008() {
	engine := gin.Default()
	// 1.HTTP重定向
	engine.GET("/nba", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "http://china.nba.cn")
	})
	// 2.路由重定向
	engine.GET("/testA", func(c *gin.Context) {
		c.Request.URL.Path = "/testB"
		engine.HandleContext(c)
	})

	engine.GET("/testB", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "testB",
		})
	})

	engine.Run()
}

// TestGin007 文件上传
func TestGin007() {
	router := gin.Default()
	// 1.加载页面
	router.LoadHTMLFiles("./templates/upload.html")
	router.GET("/upload", func(c *gin.Context) {
		c.HTML(http.StatusOK, "upload.html", nil)
	})

	// 处理multipart forms提交文件时默认的内存限制是32 MiB, 手动修改 router.MaxMultipartMemory = 8 << 20  // 8 MiB
	router.POST("/upload_file", func(c *gin.Context) {
		// 2.单个文件，读取前端上传的文件内容
		file, err := c.FormFile("file01")
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
			})
			return
		}

		// 3.保存文件
		dst := fmt.Sprintf("./img/%s", file.Filename)
		c.SaveUploadedFile(file, dst)
		c.JSON(http.StatusOK, gin.H{
			"message": fmt.Sprintf("'%s' upload success...", file.Filename),
		})
	})

	// 多个文件上传
	router.POST("/upload_files", func(c *gin.Context) {
		form, _ := c.MultipartForm()
		files := form.File["file"]
		for index, file := range files {
			log.Println(file.Filename)
			dst := fmt.Sprintf("./img/%s_%d", file.Filename, index)
			// 上传文件到指定的目录
			c.SaveUploadedFile(file, dst)
		}
		c.JSON(http.StatusOK, gin.H{
			"message": fmt.Sprintf("%d files upload success...", len(files)),
		})
	})

	router.Run()
}

// Student TestGin006 [参数绑定]
// ShouldBind会按照下面的顺序解析请求中的数据完成绑定：
// 如果是 GET 请求，只使用 Form 绑定引擎（query）。
// 如果是 POST 请求，首先检查 content-type 是否为 JSON 或 XML，然后再使用 Form（form-data）
type Student struct {
	UserName     string `form:"user_name" json:"user_name" binding:"required"`
	UserPassword string `form:"user_password" json:"user_password" binding:"required"`
}

func TestGin006() {
	router := gin.Default()
	// 1.绑定JSON的示例 ({"user_name": "zhupeng", "user_password": "123456"})
	router.POST("/loginjson", func(c *gin.Context) {
		var stu Student
		if err := c.ShouldBind(&stu); err == nil {
			fmt.Printf("stu: %v \n", stu)
			c.JSON(http.StatusOK, gin.H{
				"user_name":     stu.UserName,
				"user_password": stu.UserPassword,
			})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
	})

	// 2.绑定form表单示例 (user=q1mi&password=123456)
	router.POST("/loginForm", func(c *gin.Context) {
		var stu Student
		// ShouldBind()会根据请求的Content-Type自行选择绑定器
		if err := c.ShouldBind(&stu); err == nil {
			c.JSON(http.StatusOK, gin.H{
				"user":     stu.UserName,
				"password": stu.UserPassword,
			})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
	})

	// 3.绑定QueryString示例 (/loginQuery?user_name=q1mi&user_password=123456)
	router.GET("/loginQuery", func(c *gin.Context) {
		var stu Student
		// ShouldBind()会根据请求的Content-Type自行选择绑定器
		if err := c.ShouldBind(&stu); err == nil {
			c.JSON(http.StatusOK, gin.H{
				"user":     stu.UserName,
				"password": stu.UserPassword,
			})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
	})

	router.Run(":8080")
}

// TestGin005 [获取path参数]                请求的参数通过URL路径传递，例如：/user/search/zhupeng/23
func TestGin005() {
	router := gin.Default()
	router.GET("/user/search/:name/:age", func(c *gin.Context) {
		name := c.Param("name")
		age := c.Param("age")
		c.JSON(http.StatusOK, gin.H{
			"username": name,
			"age":      age,
		})
	})
	router.Run(":8080")
}

// TestGin004 [获取form参数]                例如向/user/login发送一个POST请求，获取请求数据的方式如下
func TestGin004() {
	router := gin.Default()
	router.POST("/user/login", func(c *gin.Context) {
		username := c.DefaultPostForm("username", "nil")
		password := c.PostForm("password")
		age, _ := c.GetPostForm("age")
		c.JSON(http.StatusOK, gin.H{
			"username": username,
			"password": password,
			"age":      age,
		})
	})
	router.Run(":8080")
}

// TestGin003 [获取querystring参数]		也就是URL中?后面携带的参数，例如：/user/search?username=小王子&address=沙河&age=1
func TestGin003() {
	router := gin.Default()
	router.GET("/user/search", func(c *gin.Context) {
		username := c.DefaultQuery("username", "unknown")
		address := c.Query("address")
		age, _ := c.GetQuery("age")
		c.JSON(http.StatusOK, gin.H{
			"username": username,
			"address":  address,
			"age":      age,
		})
	})
	router.Run(":8080")
}

// TestGin002 HTML渲染
func TestGin002() {
	// 1.创建一个默认的路由引擎
	router := gin.Default()
	// PS1:定义一个不转义相应内容的safe模板函数
	//router.SetFuncMap(template.FuncMap{
	//	"Safe": func(str string) template.HTML {
	//		return template.HTML(str)
	//	},
	//})
	// PS2:静态文件处理
	//router.Static("/static", "./static")

	// 2.加载html文件
	//router.LoadHTMLFiles("./templates/posts/index.html", "./templates/users/index.html")
	router.LoadHTMLGlob("../../templates/**/*")

	// 3.逻辑处理+渲染文件
	// 使用不同目录下名称相同的模板
	router.GET("/posts/index", func(c *gin.Context) {
		c.HTML(200, "posts/index.html", gin.H{
			"name": "朱鹏",
			"age":  23,
			"path": "/posts/index",
			//"msg": "<a href='https://liwenzhou.com'>李文周的博客</a>",
		})
	})
	router.GET("/users/index", func(c *gin.Context) {
		c.HTML(200, "users/index.html", gin.H{
			"name": "朱鹏",
			"age":  23,
			"path": "/users/index",
			//"msg": "<a href='https://liwenzhou.com'>李文周的博客</a>",
		})
	})

	// 4.启动HTTP服务
	router.Run(":8080")
}

// TestGin001 JSON渲染
func TestGin001() {
	// 1.创建一个默认的路由引擎
	router := gin.Default()
	// 2.前后端交互：当客户端以GET方法请求/hello路径时，会执行后面的匿名函数
	router.GET("/json01", func(c *gin.Context) {
		// 方式一：自己拼接JSON【 gin.H 是map[string]interface{}的缩写】
		c.JSON(200, gin.H{
			"name":    "zhupeng",
			"message": "hello",
			"age":     "23",
		})
	})

	// 方法二：使用结构体
	router.GET("/json02", func(c *gin.Context) {
		var msg struct {
			Name    string `json:"user"`
			Message string
			Age     int
		}
		msg.Name = "小王子"
		msg.Message = "Hello world!"
		msg.Age = 18
		c.JSON(http.StatusOK, msg)
	})

	// 3.启动HTTP服务，默认在127.0.0.1:8080启动服务
	router.Run(":8080")
}
