package demo09

import (
	"reflect"
	"testing"
)

// test01: 测试函数名必须以Test开头，必须接收一个*testing.T类型参数
func TestSplit(t *testing.T) {
	result := Split("a:b:c", ":")     // 程序输出的结果
	expect := []string{"a", "b", "c"} // 期望的结果
	if !reflect.DeepEqual(result, expect) {
		t.Errorf("expect:%v, result:%v", expect, result)
	}
}

// test02
func TestMoreSplit(t *testing.T) {
	got := Split("abcd", "bc")
	want := []string{"a", "d"}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("expected:%v, got:%v", want, got)
	}
}

// test03: 测试组:测试结果不明显
func TestMultiSplits(t *testing.T) {
	// 1.定义一个测试用例类型,入参、希望结果
	type test struct {
		args1  string // 入参1
		args2  string // 入参2
		expect []string
	}

	// 2.定义一个存储测试用例的切片
	tests := []test{
		{args1: "a:b:c", args2: ":", expect: []string{"a", "b", "c"}},
		{args1: "a:b:c", args2: ",", expect: []string{"a:b:c"}},
		{args1: "abcd", args2: "bc", expect: []string{"a", "d"}},
		{args1: "沙河有沙又有河", args2: "沙", expect: []string{"", "河有", "又有河"}},
	}

	// 3.遍历切片，逐一执行测试用例
	for _, tc := range tests {
		result := Split(tc.args1, tc.args2)
		if !reflect.DeepEqual(result, tc.expect) {
			t.Errorf("expected:%v, result:%v", tc.expect, result)
		}
	}

}

// test04: Go1.7+中新增了子测试
func TestMultiSplitsTwo(t *testing.T) {
	// 1.定义一个测试用例类型,入参、希望结果
	type test struct {
		args1  string // 入参1
		args2  string // 入参2
		expect []string
	}

	// 2.定义一个存储测试用例的切片
	tests := map[string]test{ // 测试用例使用map存储
		"simple":      {args1: "a:b:c", args2: ":", expect: []string{"a", "b", "c"}},
		"wrong sep":   {args1: "a:b:c", args2: ",", expect: []string{"a:b:c"}},
		"more sep":    {args1: "abcd", args2: "bc", expect: []string{"a", "d"}},
		"leading sep": {args1: "沙河有沙又有河", args2: "沙", expect: []string{"", "河有", "又有河"}},
	}

	// 3.遍历切片，逐一执行测试用例
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) { // 使用t.Run()执行子测试
			got := Split(tc.args1, tc.args2)
			if !reflect.DeepEqual(got, tc.expect) {
				t.Errorf("expected:%#v, got:%#v", tc.expect, got)
			}
		})
	}
}

// test05:性能比较函数
func benchmarkFib(b *testing.B, n int) {
	for i := 0; i < b.N; i++ {
		Fib(n)
	}
}

func BenchmarkFib1(b *testing.B)  { benchmarkFib(b, 1) }
func BenchmarkFib2(b *testing.B)  { benchmarkFib(b, 2) }
func BenchmarkFib3(b *testing.B)  { benchmarkFib(b, 3) }
func BenchmarkFib10(b *testing.B) { benchmarkFib(b, 10) }
func BenchmarkFib20(b *testing.B) { benchmarkFib(b, 20) }
func BenchmarkFib40(b *testing.B) { benchmarkFib(b, 40) }

// test06：并行测试
func BenchmarkSplitParallel(b *testing.B) {
	// b.SetParallelism(1) // 设置使用的CPU数
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			Split("沙河有沙又有河", "沙")
		}
	})
}

// 测试步骤：
// cd src/basic/demo09
// go test
// go test -v 					查看测试函数名称和运行时间
// go test -v -run="More"		-run参数对应一个正则表达式，只有函数名匹配上的测试函数才会执行
// go test -cover				查看测试覆盖率

// go test -cover -coverprofile=c.out	将覆盖率相关的记录信息输出到一个文件
// go tool cover -html=c.out			打开本地的浏览器窗口生成一个HTML报告

// go test -bench=.				执行性能测试用例
