package demo09

import "strings"

/*
Go语言中的测试依赖go test命令
	1、go test命令是一个按照一定约定和组织的测试代码的驱动程序,所有以_test.go为后缀名的源代码文件都是go test测试的一部分，不会被go build编译到最终的可执行文件中。
	2、在*_test.go文件中有三种类型的函数，单元测试函数、基准测试函数和示例函数。
		类型				格式					作用
		测试函数		函数名前缀为Test		测试程序的一些逻辑行为是否正确
		基准函数		函数名前缀为Benchmark	测试函数的性能
		示例函数		函数名前缀为Example	为文档提供示例文档
	3、go test命令会遍历所有的*_test.go文件中符合上述命名规则的函数，然后生成一个临时的main包用于调用相应的测试函数，然后构建并运行、报告测试结果，最后清理测试中生成的临时文件。
	4、每个测试函数必须导入testing包、测试函数的名字必须以Test开头
*/

func Split(s, sep string) (result []string) {
	i := strings.Index(s, sep)

	for i > -1 {
		result = append(result, s[:i])
		s = s[i+len(sep):] // 这里使用len(sep)获取sep的长度
		i = strings.Index(s, sep)
	}
	result = append(result, s)
	return
}

// Fib 是一个计算第n个斐波那契数的函数
func Fib(n int) int {
	if n < 2 {
		return n
	}
	return Fib(n-1) + Fib(n-2)
}
