package main

import "fmt"

/*
接口（interface）：
	1、在Go语言中接口是一种类型，一种抽象的类型!!!
	2、相较于之前章节中讲到的那些具体类型更注重“我是谁”，接口类型更注重“我能做什么”的问题
	3、接口定义了一个对象的行为规范，只定义规范不实现，由具体的对象来实现规范的细节
	4、一个类型实现多个接口、多种类型实现同一接口
*/
func main() {
	//Checkout(&ZhiFuBao{}) // 之前调用支付宝支付
	//Checkout(&WeChat{})   // 现在支持使用微信支付

	// 1.指针接收者实现接口: 只可以接收结构体类型的变量
	var p Payer
	var z = ZhiFuBao{}
	// p = z
	p = &z
	p.Pay(100)

	// 2.值接收者实现接口： 不管是结构体类型还是对应的结构体指针类型的变量都可以赋值给该接口变量
	var h = HuaWei{}
	p = &h
	p = h
	p.Pay(100)

	// 3. 空接口：空接口是指没有定义任何方法的接口类型。因此任何类型都可以视为实现了空接口，空接口类型的变量可以存储任意类型的值
	nullInterface003()
}

func nullInterface003() {
	var x interface{}
	x = "你好" // 字符串型
	fmt.Printf("type:%T value:%v\n", x, x)
	x = 100 // int型
	fmt.Printf("type:%T value:%v\n", x, x)
	x = true // 布尔型
	fmt.Printf("type:%T value:%v\n", x, x)
	x = ZhiFuBao{} // 结构体类型
	fmt.Printf("type:%T value:%v\n", x, x)

	// 空接口的应用
	// 1.使用空接口实现可以接收任意类型的函数参数。
	show(123)
	// 2. 空接口作为map的值
	var studentInfo = make(map[string]interface{})
	studentInfo["name"] = "沙河娜扎"
	studentInfo["age"] = 18
	studentInfo["married"] = false
	fmt.Println(studentInfo)

	// 接口值可能赋值为任意类型的值，那我们如何从接口值获取其存储的具体数据呢？x.(T)
	justifyType(12)
}

// justifyType 对传入的空接口类型变量x进行类型断言:
func justifyType(x interface{}) {
	switch v := x.(type) {
	case string:
		fmt.Printf("x is a string，value is %v\n", v)
	case int:
		fmt.Printf("x is a int is %v\n", v)
	case bool:
		fmt.Printf("x is a bool is %v\n", v)
	default:
		fmt.Println("unsupport type！")
	}
}

// 空接口作为函数参数
func show(a interface{}) {
	fmt.Printf("type:%T value:%v\n", a, a)
}

// Payer 包含支付方法的接口类型
type Payer interface {
	Pay(int64)
}

// ZhiFuBao Pay接口实现一
type ZhiFuBao struct {
}

// Pay 支付宝的支付方法：指针接收者实现接口
func (z *ZhiFuBao) Pay(amount int64) {
	fmt.Printf("使用支付宝付款：%.2f元。\n", float64(amount/100))
}

// WeChat Pay接口实现二
type WeChat struct {
}

// Pay 微信的支付方法：指针接收者实现接口
func (w *WeChat) Pay(amount int64) {
	fmt.Printf("使用微信付款：%.2f元。\n", float64(amount/100))
}

// HuaWei Pay接口实现二
type HuaWei struct {
}

// Pay 华为支付方法：值接收者实现接口
func (h HuaWei) Pay(amount int64) {
	fmt.Printf("使用华为付款：%.2f元。\n", float64(amount/100))
}

// Checkout 结账
func Checkout(obj Payer) {
	// 支付100元
	obj.Pay(100)
}
