package main

import (
	"fmt"
	"math"
	"sort"
	"strings"
)

/**
Go语言基础之变量和常量
1.注意事项：
	函数外的每个语句都必须以关键字开始（var、const、func等）
	:=不能使用在函数外。
	_多用于占位，表示忽略值。

2.基本数据类型
整型: 	int8、int16、int32、int64、uint8、uint16、uint32、uint64
浮点型：	float32、float64
布尔型：  bool
字符串：  string

还有数组、切片、结构体、函数、map、通道（channel）等
*/
func main() {
	// 1. 常见变量
	testVariable001()
	// 2. 判断语句
	println(demoIfAndSwitch002(100))
	// 3. 数组、切片、map
	testArraySliceMap003()
}

func testVariable001() {
	var num = 10
	fmt.Printf("%d \n", num) // 十进制
	fmt.Printf("%b \n", num) // 二进制
	fmt.Printf("%o \n", num) // 八进制
	fmt.Printf("%x \n", num) // 十六进制
	fmt.Printf("%f\n", math.Pi)
	fmt.Printf("%.2f\n", math.Pi)

	// 强制类型转换
	s1 := "hello沙河小王子"
	//byteS1 := []byte(s1)
	byteS1 := []rune(s1)
	for _, value := range byteS1 {
		println(string(value))
	}
}

func demoIfAndSwitch002(score int) string {
	if score > 90 {
		return "great"
	} else if score > 80 && score <= 90 {
		return "good"
	} else {
		return "bad"
	}

	switch {
	case score > 90:
		return "great"
	case score > 80 && score <= 90:
		return "good"
	default:
		return "bad"
	}

	finger := 3
	switch finger {
	case 1:
		fmt.Println("大拇指")
	case 2:
		fmt.Println("食指")
	case 3:
		fmt.Println("中指")
	case 4:
		fmt.Println("无名指")
	case 5:
		fmt.Println("小拇指")
	default:
		fmt.Println("无效的输入！")
	}
	return "ok"
}

// important
func testArraySliceMap003() {
	// 数组01-定义使用
	numArray := [5]int{1, 3, 2, 23, 54}
	stringArray := [...]string{"zhupeng", "zhangsan", "lisi"}
	fmt.Println("numArray:", numArray)
	fmt.Println("stringArray:", stringArray)

	// 数组02-排序
	sort.Ints(numArray[:])
	fmt.Println("numArray:", numArray)

	// 切片01-直接初始化
	sliceA := []int{12, 323, 213, 321, 311, 321}
	fmt.Println(sliceA)

	// 切片02-基于数组获得
	sliceB := numArray[0:len(numArray)]
	fmt.Println(sliceB)

	// 切片03-基于make(类型，长度，容量)函数获得，切片容量自动扩容
	sliceC := make([]int, 2, 10)
	sliceC = append(sliceC, 1)
	sliceC = append(sliceC, 2)
	fmt.Println(sliceC) // [0 0 1 2]
	sliceC[0] = 888
	sliceC[1] = 999
	fmt.Println(sliceC) //[888 999 1 2]
	println(len(sliceC), cap(sliceC))

	// 切片04-追加元素
	a := []int{1, 2, 3}
	var b []int
	b = append(b, a...)
	b = append(b, 4, 5, 6)
	fmt.Println("b: ", b)

	// 切片05-切片copy
	c := []int{1, 2, 3}
	d := make([]int, 3, 3)
	copy(d, c)
	fmt.Println("c: ", c, "d:", d)

	// 切片05-切片下标index的元素:	append(f[0:index], f[index:]...)
	f := []int{1, 2, 3, 4, 5, 6}
	f = append(f[0:2], f[3:]...)
	fmt.Println("f: ", f)

	// Map01-声明，未初始化
	var map01 map[string]int
	fmt.Println("map01 is nil: ", map01 == nil)

	// Map02-声明，初始化
	map02 := map[string]int{
		"user01": 100,
		"user02": 99,
	}
	fmt.Println("map02: ", map02)

	// map03-make函数
	map03 := make(map[string]int, 8)
	map03["user01"] = 90
	map03["user02"] = 100
	map03["user03"] = 110
	map03["user04"] = 120
	map03["user05"] = 130
	map03["user06"] = 130
	map03["user07"] = 130
	map03["user08"] = 130
	map03["user09"] = 130
	map03["user10"] = 130

	// map04-常用方法
	// 判断map是否存在该key
	value, ok := map03["user02"]
	fmt.Println("ok: ", ok)
	if ok {
		fmt.Println("map03[\"user01\"]: ", value)
	} else {
		fmt.Println("not exists...")
	}

	// 删除key
	delete(map03, "user05")

	// map遍历，默认无序
	for key, value := range map03 {
		fmt.Println(key, ":", value)
	}

	// map遍历，保证有序：对key排序后遍历
	println()
	sortMap(map03)

	// map05-统计字符串中每一个单词出现的次数
	countWord()

	// map06-统计字符串中每一个字符出现的次数
	str := "how do you do"
	res := countChar(str)
	for key, value := range res {
		fmt.Println(key, ":", value)
	}
}

func countChar(str string) map[string]int {
	// 0. 空格不统计
	str = strings.ReplaceAll(str, " ", "")
	// 1. 定义map
	res := make(map[string]int, 26)
	// 2. 字符串遍历
	for _, element := range str {
		value, ok := res[string(element)]
		if ok {
			res[string(element)] = value + 1
		} else {
			res[string(element)] = 1
		}
	}
	return res
}

func countWord() {
	str := "how do you do"
	// 1. 定义map
	map01 := make(map[string]int, 10)
	// 2. 字符串的单词
	words := strings.Split(str, " ")
	for _, word := range words {
		value, ok := map01[word]
		if ok {
			map01[word] = value + 1
		} else {
			map01[word] = 1
		}
	}
	// 3.输出
	for key, value := range map01 {
		fmt.Println(key, ":", value)
	}
}

func sortMap(map03 map[string]int) {
	// 1.取出map的key放到切片
	keys := make([]string, 0, 100)
	for key, _ := range map03 {
		keys = append(keys, key)
	}

	// 2.对key排序
	sort.Strings(keys)

	// 3.根据key到map中取值
	for _, element := range keys {
		key := element
		value := map03[element]
		fmt.Println(key, ":", value)
	}
}
