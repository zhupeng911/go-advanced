package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	// 1. 结构体实例化
	// structInstance001()

	// 2. 模拟实现构造函数
	person := newPersonInstance("zhangsan", "hangzhou", 24)
	fmt.Println("person: ", person)
	fmt.Println("person.name: ", person.name)

	// 3. 方法接收者
	person.Dream()
	person.UpdatePerson("lisi", "南京")
	fmt.Println("person: ", person)
	var m1 MyInt
	m1.SayHello() //Hello, 我是一个int

	// 4.嵌套结构体001
	user1 := User{
		Name:   "小王子",
		Gender: "男",
		Address: Address{
			Province:   "山东",
			City:       "威海",
			CreateTime: "2020-10-11",
		},
		Email: Email{
			Account:    "1726711912",
			CreateTime: "2022-10-11",
		},
	}
	fmt.Println("user1: ", user1)

	// user3.CreateTime = "1996-10-11" 		//ambiguous selector user3.CreateTime
	user1.Address.CreateTime = "1996-10-11" //指定Address结构体中的CreateTime
	user1.Email.CreateTime = "1996-10-11"   //指定Email结构体中的CreateTime
	fmt.Println("user1: ", user1)

	// 5. 结构体的继承
	d1 := Dog{
		Feet: 4,
		Animal: &Animal{ //注意嵌套的是结构体指针
			Name: "乐乐",
		},
	}
	d1.wang() // 乐乐会汪汪汪~
	d1.move() // 乐乐会动！

	// 6. 结构体与JSON序列化
	jsonSerialization()
}

func jsonSerialization() {
	// 1.准备结构体数据
	c := &Class{
		Title:    "101",
		Students: make([]*Student, 0, 200),
	}
	for i := 0; i < 10; i++ {
		stu := &Student{
			Name:   fmt.Sprintf("stu%02d", i),
			Gender: "男",
			ID:     i,
		}
		c.Students = append(c.Students, stu)
	}
	// 2.JSON序列化：结构体-->JSON格式的字符串
	data, _ := json.Marshal(c)
	fmt.Println("json: ", string(data))

	// 3. JSON反序列化：JSON格式的字符串-->结构体
	c1 := &Class{}
	err := json.Unmarshal([]byte(data), c1)
	if err != nil {
		fmt.Println("json unmarshal failed!")
		return
	}
	fmt.Printf("%#v\n", c1)
}

func structInstance001() {
	// 结构体实例化001
	var person001 Person
	person001.name = "zhangsan"
	person001.city = "hangzhou"
	person001.age = 23

	// 结构体实例化002
	person002 := Person{
		name: "里斯",
		city: "南京",
		age:  25,
	}

	// 结构体实例化003
	person003 := Person{"alice", "beijing", 35}
	fmt.Println("person001:", person001)
	fmt.Println("person002:", person002)
	fmt.Println("person003:", person003)
	fmt.Println("person001.name:", person001.name)

	// 匿名结构体
	var user struct {
		name string
		age  int
	}
	user.name = "王武"
	user.age = 24
	fmt.Println("匿名结构体-user: ", user)

	// 结构体内存布局: 结构体占用一块连续的内存, 空结构体是不占用空间的
	fmt.Printf("person001.name %p\n", &person001.name)
	fmt.Printf("person001.city %p\n", &person001.city)
	fmt.Printf("person001.age %p\n", &person001.age)
}

/*
结构体：Go语言中通过struct来实现面向对象
构造函数：
	Go语言的结构体没有构造函数，我们可以自己实现。因为struct是值类型，如果结构体比较复杂的话，值拷贝性能开销会比较大，所以该构造函数返回的是结构体指针类型
*/

type Person struct {
	name string
	city string
	age  int
}

// 2. 模拟实现构造函数
func newPersonInstance(name string, city string, age int) *Person {
	return &Person{
		name: name,
		city: city,
		age:  age,
	}
}

// Dream 3. 方法和接收者，表明该方法属于哪一个结构体
// 方法与函数的区别是：函数不属于任何类型，方法属于特定的类型。
// 值类型的接收者
// 当方法作用于值类型接收者时，Go语言会在代码运行时将接收者的值复制一份，无法修改接收者变量本身。
func (p Person) Dream() {
	fmt.Println("p: ", p)
	fmt.Printf("%s的梦想是学好Go语言！\n", p.name)
}

// UpdatePerson 指针类型的接收者
// 指针类型的接收者由一个结构体的指针组成，由于指针的特性，调用方法时修改接收者指针的任意成员变量，在方法结束后，修改都是有效的。
func (p *Person) UpdatePerson(newName string, newCity string) {
	p.name = newName
	p.city = newCity
}

// MyInt 任意类型添加方法
// 在Go语言中，接收者的类型可以是任何类型，不仅仅是结构体，任何类型都可以拥有方法。
// 举个例子，我们基于内置的int类型使用type关键字可以定义新的自定义类型，然后为我们的自定义类型添加方法。
type MyInt int

func (m MyInt) SayHello() {
	fmt.Println("Hello, 我是一个int。")
}

// Address 嵌套结构体：一个结构体中可以嵌套包含另一个结构体或结构体指针
type Address struct {
	Province   string
	City       string
	CreateTime string
}

type Email struct {
	Account    string
	CreateTime string
}

type User struct {
	Name    string
	Gender  string
	Address // 嵌套匿名字段, 等同于 Address Address
	Email   // 嵌套匿名字段, 等同于 Email Email
}

// Animal 5. 结构体的“继承”: Go语言中使用结构体也可以实现其他编程语言中面向对象的继承
type Animal struct {
	Name string
}

func (a *Animal) move() {
	fmt.Printf("%s会动！\n", a.Name)
}

type Dog struct {
	Feet    int8
	*Animal // 通过嵌套匿名结构体实现继承,继承Animal类的方法
}

func (d *Dog) wang() {
	fmt.Printf("%s会汪汪汪~\n", d.Name)
}

// Student 6.结构体与JSON序列化
// JSON(JavaScript Object Notation) 是一种轻量级的数据交换格式。
// 易于人阅读和编写。同时也易于机器解析和生成。JSON键值对是用来保存JS对象的一种方式，
// 键/值对组合中的键名写在前面并用双引号""包裹，使用冒号:分隔，然后紧接着值；多个键值之间使用英文,分隔。
type Student struct {
	ID     int `json:"id"` //通过指定tag实现json序列化该字段时的key
	Gender string
	Name   string
}

type Class struct {
	Title    string
	Students []*Student
}
