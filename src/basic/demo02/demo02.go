package main

import (
	"fmt"
	"strings"
)

func main() {
	// 1.闭包
	// testBiBao001()

	// 2.defer
	//testDefer002()

	// 3.指针
	testPointer003()
}

/*
闭包指的是一个函数和与其相关的引用环境组合而成的实体。简单来说，闭包=函数+引用环境
可以理解为返回值是一个函数，并且引用了其他变量
*/
func testBiBao001() {
	f := biBao001(10)  // x=10
	fmt.Println(f(20)) //  y=20 x=30
	fmt.Println(f(30)) //  y=30 x=60

	jpgFunc := makeSuffixFunc(".jpg")
	fmt.Println(jpgFunc("test")) //test.jpg
	txtFunc := makeSuffixFunc(".txt")
	fmt.Println(txtFunc("test.txt")) //test.txt

	f1, f2 := calculate(10)
	fmt.Println(f1(1), f2(2)) //11 9
	fmt.Println(f1(3), f2(4)) //12 8
}

// 案例一：
func biBao001(x int) func(int) int {
	return func(y int) int {
		x += y
		return x
	}
}

// 案例二：判断入参fileName是否以suffix为结尾,不是则添加
func makeSuffixFunc(suffix string) func(string) string {
	return func(fileName string) string {
		if strings.HasSuffix(fileName, suffix) {
			return fileName
		}
		return fileName + suffix
	}
}

// 案例三
func calculate(base int) (func(int) int, func(int) int) {
	add := func(i int) int {
		base += i
		return base
	}

	sub := func(i int) int {
		base -= i
		return base
	}
	return add, sub
}

/*
	由于defer语句延迟调用的特性，所以defer语句能非常方便的处理资源释放问题。
	比如：资源清理、文件关闭、解锁及记录时间等
*/
func testDefer002() {
	fmt.Println("start")
	defer fmt.Println(1)
	defer fmt.Println(2)
	defer fmt.Println(3)
	fmt.Println("end")
}

/*
指针地址:
	每个变量在运行时都拥有一个地址，这个地址代表变量在内存中的位置
指针类型:
	Go语言中的值类型（int、float、bool、string、array、struct）都有对应的指针类型，如：*int、*int64、*string等
指针取值:
	在对普通变量使用&操作符取地址后会获得这个变量的指针，然后可以对指针使用*操作
	[&（取地址）和*（根据地址取值）]
*/
func testPointer003() {
	a := 10
	b := &a
	fmt.Printf("a:%d   &a:%p\n", a, &a) // a:10 ptr:0xc00001a078
	fmt.Printf("b:%p   *b:%d\n", b, *b) // b:0xc00001a078 10
	fmt.Printf("a:%T   b:%T\n", a, b)   // a:int   b:*int

	// new and make
	//二者都是用来做内存分配的。
	//make只用于slice、map以及channel的初始化，返回的还是这三个引用类型本身；
	//而new用于类型的内存分配，并且内存对应的值为类型零值，返回的是指向类型的指针。
	c := new(int)
	d := new(bool)
	fmt.Printf("%T\n", c) // *int
	fmt.Printf("%T\n", d) // *bool
	fmt.Println(*c)       // 0
	fmt.Println(*d)       // false

	var map01 map[string]int
	map01 = make(map[string]int, 10)
	map01["沙河娜扎"] = 100
	fmt.Println(map01)
}
