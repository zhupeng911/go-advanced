package pck02

import "fmt"
import "go_advanced/src/basic/demo04/pck01"

func SayHi() {
	var myName = "demo05"
	fmt.Println(myName)
	pck01.SayHi()
}

func init() {
	fmt.Println("demo05..init...")
}
