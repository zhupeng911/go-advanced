package pck01

import "fmt"

// 定义 package packagename
// 1.packagename包名可以不与文件夹的名称一致，不能包含 - 符号，最好与其实现的功能相对应
// 2.文件夹下面直接包含的文件只能归属一个包，同一个包的文件不能在多个文件夹下。

// 首字母小写，对外不可见(只能在当前包内使用)
var num = 100

type person struct {
	name string
	Age  int
}

// Mode 首字母大写，对外可见(可在其它包中使用)
const Mode = 1

func Add(x, y int) int {
	return x + y
}

func SayHi() {
	var myName = "demo04"
	fmt.Println(myName)
}

// 这种特殊的函数不接收任何参数也没有任何返回值，我们也不能在代码中主动调用它。
// 当程序启动的时候，init函数会按照它们声明的顺序自动执行。
func init() {
	fmt.Println("demo04..init...")
}
