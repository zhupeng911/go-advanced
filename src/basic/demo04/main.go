package main

import "go_advanced/src/basic/demo04/pck02"

/**
包（package）：
	Go语言中支持模块化的开发理念，在Go语言中使用包来支持代码模块化和代码复用。
	一个包是由一个或多个Go源码文件（.go结尾的文件）组成，是一种高级的代码复用方案，
*/
func main() {
	pck02.SayHi()
}

/*
go module
go mod init			初始化项目依赖，生成go.mod文件
go mod download		根据go.mod文件下载依赖
go mod tidy			比对项目文件中引入的依赖与go.mod进行比对
go mod graph		输出依赖关系图
go mod edit			编辑go.mod文件
go mod vendor		将项目的所有依赖导出至vendor目录
go mod verify		检验一个依赖包是否被篡改过
go mod why			解释为什么需要某个依赖


GOPROXY
	这个环境变量主要是用于设置 Go 模块代理（Go module proxy），
	其作用是用于使 Go 在后续拉取模块版本时能够脱离传统的 VCS 方式，直接通过镜像站点来快速拉取
	go env -w GOPROXY=https://goproxy.cn,direct
*/
