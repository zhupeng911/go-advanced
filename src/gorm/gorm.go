package main

// 参考文档
// https://www.liwenzhou.com/posts/Go/gorm/
// https://www.liwenzhou.com/posts/Go/gorm_crud/

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"time"
)

func main() {
	//TestGorm001()
	//TestGorm002()
	//TestGorm003()
	TestGorm004()
}

// TestGorm004 删除记录-请确保主键字段有值，GORM会通过主键去删除记录，如果主键为空，GORM会删除该model的所有记录
//推荐：
//普通场景：利用Where限定删除条件，不建议太复杂；
//软删除：在实际项目中，不太建议用硬删除的方式，而是用软删除，即更新一个标记字段。
// [Delete]
func TestGorm004() {
	// 1.连接数据库mysql
	db, err := gorm.Open("mysql", "root:zhupeng123@(127.0.0.1:3306)/go_db_1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	var user DFUser
	db.First(&user)

	// 1.删除现有记录
	db.Delete(&user)
	// DELETE from df_user where id=10;

	// 2.批量删除 删除全部匹配的记录
	db.Where("user_name LIKE ?", "%朱鹏%").Delete(DFUser{})
	db.Delete(DFUser{}, "user_name LIKE ?", "%朱鹏%")
	// DELETE from df_user where user_name LIKE "%朱鹏%";

	// 3.软删除 如果一个 model 有 DeletedAt 字段，他将自动获得软删除的功能！ 当调用 Delete 方法时， 记录不会真正的从数据库中被删除， 只会将DeletedAt 字段的值会被设置为当前时间
	db.Delete(&user)
	// UPDATE df_user SET deleted_at="2013-10-29 10:23" WHERE id = 111;

	db.Where("user_age = ?", 20).Delete(&DFUser{})
	db.Delete(DFUser{}, "user_age = ?", 20)
	// UPDATE df_user SET deleted_at="2013-10-29 10:23" WHERE user_age = 20;

	// 4.物理删除 Unscoped 方法可以物理删除记录
	db.Unscoped().Delete(&user)
	// DELETE FROM df_user WHERE id=10;

	// Unscoped 方法可以查询被软删除的记录
	db.Unscoped().Where("user_age = 20").Find(&DFUser{})
	// SELECT * FROM df_user WHERE user_age = 20;
}

// TestGorm003 更新操作
// 推荐：
//普通场景：利用Select+Updates指定更新字段，利用Where指定更新条件；
//特殊场景：复杂SQL用原生SQL。
// [Save、Update、Updates、Select、Omit]
func TestGorm003() {
	// 1.连接数据库mysql
	db, err := gorm.Open("mysql", "root:zhupeng123@(127.0.0.1:3306)/go_db_1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	var user DFUser
	db.First(&user)

	// 2.更新操作
	// a、更新所有字段 	Save()默认会更新该对象的所有字段，即使你没有赋值。
	user.UserName = "update朱鹏"
	user.UserAge = 100
	db.Debug().Save(&user)
	// UPDATE `df_user` SET `created_at` = '2022-11-04 13:43:20', `updated_at` = '2022-11-04 14:34:18', `deleted_at` = NULL, `user_name` = 'update朱鹏', `user_age` = 100, `user_member_number` = 'VIP1000', `user_address` = '江苏南京', `user_birthday` = '2022-11-04 13:43:20'  WHERE `df_user`.`deleted_at` IS NUL`df_user`.`id` = 1 AND `df_user`.`user_id` = 1000

	// b、更新修改字段 	可以使用Update或者Updates
	db.Model(&user).Update("user_name", "zhupeng_update")
	// UPDATE users SET name='zhupeng123', updated_at='2013-11-17 21:34:10' WHERE id=111;

	db.Model(&user).Where("user_address = ?", "江苏南京").Update("user_name", "zhupeng_update")
	// UPDATE users SET name='zhupeng_update', updated_at='2013-11-17 21:34:10' WHERE id=111 AND user_address='江苏南京';

	db.Model(&user).Updates(map[string]interface{}{"user_name": "zhupeng_update", "user_age": 18})
	db.Model(&user).Updates(DFUser{UserName: "zhupeng_update", UserAge: 18})
	db.Model(&user).Updates(DFUser{UserName: "zhupeng_update", UserAge: 0, UserBirthday: time.Now()})
	// UPDATE users SET user_name='zhupeng_update', user_age=18, updated_at='2013-11-17 21:34:10' WHERE id=111;

	// c、更新选定字段		如果你想更新或忽略某些字段，你可以使用 Select，Omit
	db.Model(&user).Select("user_name").Updates(map[string]interface{}{"user_name": "hello", "user_age": 18})
	// UPDATE users SET user_name='hello', updated_at='2013-11-17 21:34:10' WHERE id=111;

	db.Model(&user).Omit("user_name").Updates(map[string]interface{}{"user_name": "hello", "user_age": 18})
	// UPDATE users SET user_age=18, updated_at='2013-11-17 21:34:10' WHERE id=111;

	// d、批量更新
	db.Where("id IN (?)", []int{10, 11}).Updates(map[string]interface{}{"user_name": "hello", "user_age": 18})
	// UPDATE users SET user_name='hello', user_age=18 WHERE id IN (10, 11);

	// 使用 `RowsAffected` 获取更新记录总数
	rowsAffected := db.Model(DFUser{}).Updates(DFUser{UserName: "hello", UserAge: 18}).RowsAffected
	fmt.Printf("rowsAffected: %v", rowsAffected)

}

// TestGorm002 查询操作
//推荐：
//普通场景：简单查询用Find+Where的函数结合实现，结合Limit+Offset+Order实现分页等高频功能；
//追求性能：可以引入Select避免查询所有字段，但会导致返回结果部分字段不存在的奇怪现象，需要权衡；
//复杂查询：例如Join+子查询等，推荐使用下面的原生SQL，用GORM拼接的体验并不好。
func TestGorm002() {
	// 1.连接数据库mysql
	db, err := gorm.Open("mysql", "root:zhupeng123@(127.0.0.1:3306)/go_db_1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 2.查询操作
	var user DFUser

	// a、一般查询
	db.First(&user)    // 根据主键查询第一条记录 SELECT * FROM df_user ORDER BY id LIMIT 1;
	db.Last(&user)     // 根据主键查询最后一条记录 SELECT * FROM df_user ORDER BY id DESC LIMIT 1;
	db.Take(&user)     // 随机获取一条记录 SELECT * FROM df_user LIMIT 1;
	db.Find(&user)     // 查询所有的记录 SELECT * FROM df_user;
	db.First(&user, 1) // 查询指定的某条记录(仅当主键为整型时可用)SELECT * FROM df_user WHERE id = 1;

	// b、Where 条件
	db.Where("user_name = ?", "朱鹏").Find(&user)
	// SELECT * FROM df_user WHERE user_name = '朱鹏';

	db.Where("user_name <> ?", "朱鹏").Find(&user)
	// SELECT * FROM df_user WHERE user_name <> '朱鹏';

	db.Where("user_name IN (?)", []string{"朱鹏", "朱鹏1"}).Find(&user)
	// SELECT * FROM df_user WHERE user_name in ('朱鹏','朱鹏1');

	db.Where("user_name LIKE ?", "%朱%").Find(&user)
	// SELECT * FROM df_user WHERE user_name LIKE '%朱%';

	db.Where("user_name = ? AND user_age >= ?", "朱鹏", "23").Find(&user)
	// SELECT * FROM df_user WHERE user_name = '朱鹏' AND user_age >= 23;

	db.Where("updated_at > ?", time.Now()).Find(&user)
	// SELECT * FROM df_user WHERE updated_at > '2000-01-01 00:00:00';

	db.Where("created_at BETWEEN ? AND ?", time.Now(), time.Now()).Find(&user)
	// SELECT * FROM df_user WHERE created_at BETWEEN '2000-01-01 00:00:00' AND '2000-01-08 00:00:00';

	// c、Struct & Map & 切片查询
	db.Where(&DFUser{UserName: "朱鹏", UserAge: 23}).First(&user)
	// SELECT * FROM df_user WHERE user_name = "朱鹏" AND user_age = 23 LIMIT 1;

	db.Where(map[string]interface{}{"user_name": "朱鹏", "user_age": 23}).Find(&user)
	// SELECT * FROM df_user WHERE user_name = "朱鹏" AND user_age = 20;

	db.Where([]int64{1, 2, 3}).Find(&user)
	// SELECT * FROM df_user WHERE id IN (20, 21, 22);

	// d、Not 条件
	db.Not("user_name", []string{"朱鹏", "朱鹏1"}).Find(&user)
	// SELECT * FROM df_user WHERE user_name NOT IN ("朱鹏", "朱鹏1");

	db.Not([]int64{1, 2, 3}).First(&user)
	// SELECT * FROM df_user WHERE id NOT IN (1,2,3);

	// e、Or条件
	db.Where("user_name = ?", "朱鹏").Or("user_name = ?", "朱鹏1").Find(&user)
	// SELECT * FROM df_user WHERE user_name = '朱鹏' OR user_name = '朱鹏1';
	db.Where("user_name = '朱鹏'").Or(DFUser{UserName: "朱鹏1"}).Find(&user)
	// SELECT * FROM df_user WHERE user_name = '朱鹏' OR user_name = '朱鹏1';

	// f、高级子查询[选择字段、排序、总数]
	db.Select("user_name, user_age").Find(&user)
	db.Select([]string{"user_name", "user_age"}).Find(&user)
	// SELECT user_name, user_age FROM df_user;

	db.Select("COALESCE(user_age,?)", 42).Rows()
	// SELECT COALESCE(user_age,'42') FROM df_user;

	db.Order("user_age desc, user_name").Find(&user)
	// SELECT * FROM df_user ORDER BY user_age desc, user_name;

	db.Order("user_age desc").Order("user_name").Find(&user)
	// SELECT * FROM df_user ORDER BY user_age desc, user_name;

	var count int
	db.Table("df_user").Count(&count)
	// SELECT count(*) FROM df_user;

	db.Where("user_name = ?", "朱鹏").Or("user_name = ?", "朱鹏2").Find(&user).Count(&count)
	// SELECT * from df_user WHERE user_name = '朱鹏' OR user_name = '朱鹏 2'; (df_user)
	// SELECT count(*) FROM df_user WHERE user_name = '朱鹏' OR user_name = '朱鹏 2'; (count)

	db.Model(&DFUser{}).Where("user_name = ?", "朱鹏").Count(&count)
	// SELECT count(*) FROM df_user WHERE user_name = '朱鹏'; (count)

	db.Table("df_user").Select("count(distinct(user_name))").Count(&count)
	// SELECT count( distinct(user_name) ) FROM deleted_df_user; (count)
}

type DFUser struct {
	gorm.Model              // gorm.Model是一个包含了ID, CreatedAt, UpdatedAt, DeletedAt四个字段的Golang结构体
	UserId           uint64 `gorm:"column:user_id;type:int;primary_key;unique;not null"`
	UserName         string `gorm:"column:user_name;type:varchar(255)"`
	UserAge          int    `gorm:"column:user_age;default:0"`
	UserMemberNumber string `gorm:"unique_index;not null"` // 设置会员号（member number）唯一并且不为空
	UserAddress      string `gorm:"index:addr"`            // 给UserAddress字段创建名为addr的索引
	UserBirthday     time.Time
	IgnoreMe         int `gorm:"-"` // 忽略本字段
}

// TableName  设置表名001
func (DFUser) TableName() string {
	return "df_user"
}

// TestGorm001 定义模型Model-数据库表	在使用ORM工具时，通常我们需要在代码中定义模型Models与数据表进行映射
func TestGorm001() {
	// 1.连接数据库mysql
	db, err := gorm.Open("mysql", "root:zhupeng123@(127.0.0.1:3306)/go_db_1?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 2.创建表
	//db.AutoMigrate(&DFUser{})					 // 自动迁移【结构体与表对应，类似于JPA】
	//db.Table("df_user").CreateTable(&DFUser{}) // 手动设置表名002
	//db.SingularTable(true)					 // 禁用默认表名的复数形式，如果置为 true，则 `User` 的默认表名是 `user`
	db.CreateTable(&DFUser{})

	// 3.添加数据
	dfUser01 := DFUser{UserId: 1000, UserName: "朱鹏", UserAge: 23, UserMemberNumber: "VIP1000", UserAddress: "江苏南京", UserBirthday: time.Now()}
	dfUser02 := DFUser{UserId: 1001, UserName: "朱1", UserAge: 24, UserMemberNumber: "VIP1001", UserAddress: "江苏南京", UserBirthday: time.Now()}
	dfUser03 := DFUser{UserId: 1002, UserName: "朱2", UserAge: 24, UserMemberNumber: "VIP1002", UserAddress: "江苏南京", UserBirthday: time.Now()}
	dfUser04 := DFUser{UserId: 1003, UserName: "朱3", UserAge: 24, UserMemberNumber: "VIP1003", UserAddress: "江苏南京", UserBirthday: time.Now()}
	db.Create(&dfUser01)
	db.Create(&dfUser02)
	db.Create(&dfUser03)
	db.Create(&dfUser04)

	// 4.查询
	var user DFUser
	db.Table("df_user").First(&user)
	fmt.Printf("user: %v", user)

	// 更新
	db.Model(&user).Update("name", "朱鹏update")

	// 删除
	delUser := DFUser{UserId: 1000}
	db.Delete(&delUser)
}
